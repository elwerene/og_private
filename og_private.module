<?php

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\node\NodeInterface;
use Drupal\og\Og;

/**
 * Implements hook_module_implements_alter().
 */
function og_private_module_implements_alter(&$implementations, $hook) {
  if ($hook === 'node_access') {
    unset($implementations['nodetype_access']);
  }
}

/**
 * Implements hook_node_access().
 */
function og_private_node_access(NodeInterface $node, $op, AccountInterface $account) {
  if ($op === 'view' || $op === 'edit') {
    // We switched off that implementation and kinda decorate it here.
    $originalAccessResult = nodetype_access_node_access($node, $op, $account)
      ?? AccessResult::neutral();
    $originalAccess = !$originalAccessResult->isForbidden();
    // Sorry, a lot of negations here. Effectively this is OR access.
    // But beware, ::orIf(), it won't work for AccessForbidden.
    $ogAccess = _og_private_is_member($node, $account);
    $access = $ogAccess || $originalAccess;
    $accessResult = AccessResult::forbiddenIf(!$access)
      ->addCacheableDependency($originalAccessResult)
      ->addCacheContexts(['og_membership_state']);
    return $accessResult;
  }
}

/**
 * @param \Drupal\Core\Entity\EntityInterface $entity
 *
 * @return bool|null
 */
function _og_private_is_member(EntityInterface $entity, AccountInterface $account) {
  if (Og::isGroup($entity->getEntityTypeId(), $entity->bundle())) {
    return Og::isMember($entity, $account);
  }
  return NULL;
}
