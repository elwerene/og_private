# OG Private

A hacky solution to restrict all Organic Groups view/edit access to members of that group.
No group permissions or configuration.

This **only** restricts access for node/X pages, **not** for node listings.

It requires and plays nice with nodetype_access in the sense that user can access a node
if they have nodetype access **or** are group member.

This is a simple solution for now and will never have a stable release.
The successor will likely be based on entity_unified_access. 
